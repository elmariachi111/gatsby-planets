import React from 'react'
import { Link, graphql } from 'gatsby'

import Layout from '../components/layout'

class Ordering extends React.Component {
  render() {
    return (
      <div>
        sort by:
        <a onClick={() => this.props.onChange('order')}>order</a> |
        <a onClick={() => this.props.onChange('radius')}>radius</a>
      </div>
    )
  }
}

class IndexPage extends React.Component {
  constructor(props) {
    super(props)
    this.state = {
      sortby: 'order',
    }
  }

  render() {
    const planets = this.props.data.planets.edges.map(n => n.node.frontmatter)

    planets.sort((a, b) => {
      if (a[this.state.sortby] < b[this.state.sortby]) return -1
      else if (a[this.state.sortby] > b[this.state.sortby]) return 1
      else return 0
    })

    return (
      <Layout>
        <h2>all the planets</h2>
        <Ordering onChange={ordering => this.setState({ sortby: ordering })} />
        <ul>
          {planets.map(planet => (
            <li key={`planet-${planet.path}`}>
              {planet[this.state.sortby]}:{' '}
              <Link to={planet.path}>{planet.title}</Link>
            </li>
          ))}
        </ul>
      </Layout>
    )
  }
}

export default IndexPage

export const query = graphql`
  query {
    planets: allMarkdownRemark(
      sort: { order: ASC, fields: frontmatter___order }
    ) {
      edges {
        node {
          frontmatter {
            title
            path
            order
            radius
          }
        }
      }
    }
  }
`
